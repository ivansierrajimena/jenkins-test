import argparse

VER_DICT = {"VER_MAJOR": 0, "VER_MINOR": 1, "VER_REVISION": 2, "VER_BUILD": 3}

def main():
    parser = argparse.ArgumentParser(description = "Auto-version upgrade for APB firmware")
    parser.add_argument("workingDir")
    parser.add_argument("scope")
    param = parser.parse_args()

    scope: int = 4
    for item in VER_DICT.items():
        if param.scope == item[0]:
            scope = item[1]

    if scope > 3:
        return
    else:
        changeVersion(param.workingDir, scope)

def changeVersion(targetDir: str, scope: int = 3):
    """
    Change the version of the project
    """

    ver_major: int
    ver_minor: int
    ver_revision: int
    ver_build: int

    try:

        lines: list[str]
        with open(targetDir + '\\include\\Config.h', "r") as f:
            lines = f.readlines()

        for line in lines:
            if "VER_MAJOR" in line:
                ver_major = int(line.split(" ")[2].split("\n")[0])
            elif "VER_MINOR" in line:
                ver_minor = int(line.split(" ")[2].split("\n")[0])
            elif "VER_REVISION" in line:
                ver_revision = int(line.split(" ")[2].split("\n")[0])
            elif "VER_BUILD" in line:
                ver_build = int(line.split(" ")[2].split("\n")[0])

        print("Current version: {}.{}.{}.{}".format(ver_major, ver_minor, ver_revision, ver_build))

        if scope == VER_DICT["VER_MAJOR"]:
            ver_major += 1
            ver_minor = 0
            ver_revision = 0
            ver_build = 0
        elif scope == VER_DICT["VER_MINOR"]:
            ver_minor += 1
            ver_revision = 0
            ver_build = 0
        elif scope == VER_DICT["VER_REVISION"]:
            ver_revision += 1
            ver_build = 0
        elif scope == VER_DICT["VER_BUILD"]:
            ver_build += 1

        print("Changing project version to: {}.{}.{}.{}".format(ver_major, ver_minor, ver_revision, ver_build))

        for index in range(0, len(lines)):
            if "VER_MAJOR" in lines[index]:
                lines[index] = "#define VER_MAJOR {}\n".format(ver_major)
            elif "VER_MINOR" in lines[index]:
                lines[index] = "#define VER_MINOR {}\n".format(ver_minor)
            elif "VER_REVISION" in lines[index]:
                lines[index] = "#define VER_REVISION {}\n".format(ver_revision)
            elif "VER_BUILD" in lines[index]:
                lines[index] = "#define VER_BUILD {}\n".format(ver_build)

        with open(targetDir + '\\include\\Config.h', "w") as f:
            f.writelines(lines)

        print("Version updated!")

    except:
        print("Unable to upgrade the project version...")

if __name__ == "__main__":
    main()