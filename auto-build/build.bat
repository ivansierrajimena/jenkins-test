:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                                                                                                                   ::
::  AUTO-BUILD BATCH SCRIPT                                                                                          ::
::                                                                                                                   ::
::  @author: isierra@genasys.com                                                                                     ::
::  @date: 2021/09/01                                                                                                ::
::  @copyright: Genasys.inc (c) 2021                                                                                 ::
::                                                                                                                   ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

@echo off

:: Get the current date and time
set year=%date:~6,4%
set month=%date:~3,2%
set day=%date:~0,2%
set hour=%time:~0,2%
set hour=%hour: =0%
set minute=%time:~3,2%

:: Setting a new working directory
set targetDir=%1
IF exist %targetDir% ( echo %targetDir% exists, removing it && rd /s /q %targetDir% )
mkdir %targetDir% && echo %targetDir% created

:: Copy the temporal directory to the working directory
echo Moving from %cd% to %targetDir%...
Xcopy %cd% %targetDir% /E /H /C /I
cd %targetDir%

:: Build the project (save the output and the error messages in the same file)
pio run 1> %targetDir%build_%year%%month%%day%_%hour%%minute%.log 2>&1

:: Check the results
IF %ERRORLEVEL%==0 ( echo Build successful && EXIT /b 0 ) ELSE ( echo Build failed && EXIT /b 1 )

:: EXIT CODES
:: 0 -- Success
:: 1 -- Cannot build the project
