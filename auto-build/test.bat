:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                                                                                                                   ::
::  AUTO-UPLOAD BATCH SCRIPT                                                                                         ::
::                                                                                                                   ::
::  @author: isierra@genasys.com                                                                                     ::
::  @date: 2021/09/01                                                                                                ::
::  @copyright: Genasys.inc (c) 2021                                                                                 ::
::                                                                                                                   ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

@echo off

:: Get the current date and time
set year=%date:~6,4%
set month=%date:~3,2%
set day=%date:~0,2%
set hour=%time:~0,2%
set hour=%hour: =0%
set minute=%time:~3,2%

:: Setting a new working directory
set targetDir=%1
cd %targetDir%

:: Waits for 5 seconds and upload the project in the board
PING -n 6 127.0.0.1>nul
python stest_diagnosys.py >> %targetDir%test_%year%%month%%day%_%hour%%minute%.log

:: Check the results
IF %ERRORLEVEL%==0 ( echo Upload successful ) ELSE ( echo Upload failed && EXIT /b 1 )

:: EXIT CODES
:: 0 -- Success
:: 1 -- Cannot upload or build the project
:: 2 -- Error in the test
