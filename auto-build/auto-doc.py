import subprocess
import argparse

latexDir = "\\doc\\latex\\"
htmlDir = "\\doc\\html\\"
doxygenCmd = ["C:\\Doxygen\\bin\\doxygen.exe", "DOXYFILE"]

def main():
    parser = argparse.ArgumentParser(description = "Auto-version upgrade for APB firmware")
    parser.add_argument("workingDir")
    param = parser.parse_args()

    extractPdf(param.workingDir)

def extractPdf(targetDir: str):
    """
    Create and extract the documentation PDF of the project
    """
    # Create the documentation latex files
    print("\nGenerating the documentation...\n")
    runCmd(doxygenCmd, dir=targetDir)
    # Run the make.bat
    print("\nGenerating the PDF file...\n")
    runCmd([targetDir + latexDir + "make.bat"], showMsg=False)
    # Remove the previous .pdf and extract the .pdf
    print("\nRemoving previous files and renaming the new one...\n")
    runCmd(["del", targetDir + "\\doc\\APB 2 Firmware Documentation.pdf"], showMsg=False)
    runCmd(["move", targetDir + latexDir + "refman.pdf",
           targetDir + "\\doc"], showMsg=False)
    runCmd(["rename", targetDir + "\\doc\\refman.pdf",
           "APB 2 Firmware Documentation.pdf"], showMsg=False)
    # Remove the documentation directories
    print("\nCleaning the trash files...\n")
    runCmd(["rmdir", "/s", "/q", targetDir + latexDir], showMsg=False)
    runCmd(["rmdir", "/s", "/q", targetDir + htmlDir], showMsg=False)

def runCmd(cmd: list, dir: str = "", inputStr: str = "", showMsg: bool = True):
    """
    Executes a shell command
    """
    p = subprocess.run(cmd,
                       shell=True,
                       stdout=subprocess.PIPE if showMsg else None,
                       cwd=dir if dir != "" else None,
                       input=inputStr if inputStr != "" else None)
    if showMsg:
        print(p.stdout.decode('utf-8'))
    return p