
#!python3
"""
System test diagnosys 

Contain the command sequence to perform the tests

Created on 09 AUG 2021

@author isierra@genasys.com
@version 2.0

history

"""

#############################################################################################################
#                                                                                                           #
#  >> HOW TO DESIGN A TEST <<                                                                               #
#                                                                                                           #
#  TO DESIGN A TEST, IT IS NECESSARY TO GROUP ALL THE COMMANDS IN A LIST, THAT WILL BE READ SEQUENTIALLY.   #
#  THIS LIST MUST CONTAIN COMMAND OBJECTS, WHICH WILL DEFINE THE FOLLOWING PARAMETERS:                      #
#     1. THE COMMAND TO BE TESTED -- THIS IS THE STRING THAT AN USER WOULD WRITE IN THE CLI CONSOLE         #
#     2. THE DESCRIPTION OF THE TEST -- TO KNOW WHAT IS RUNNING ON, THIS DESCRIPTION SHOULD PORTRAY WHAT    #
#        IS THE COMMAND DOING (FOR EXAMPLE, 'RENAME EXISTING NEW FOLDER WITH ITEMS') . THIS DESCRIPTION     #
#        MUST BE AS REPRESENTATIVE AS POSSIBLE.                                                             #
#     3. THE EXPECTED OUTPUT OF THE APB -- THE RESULT THAT SHOULD GIVE THE APB TO THAT COMMAND TEST.        #
#        THIS OUTPUT CAN BE AN ERROR CODE (IF THE COMMAND IS CHECKING THE ROBUSTNESS OF SOMETHING, FOR      #
#        EXAMPLE). ALSO, THE EXPECTED OUTPUT CAN BE NONE, THIS MEANS THAT THE OUTPUT IS NOT RELEVANT        #
#        (THE COMMAND WILL BE EXECUTED ANYWAY).                                                             #
#     4. (OPTIONAL) A WAIT TIME FOR THE COMMAND -- BY DEFAULT, EVERY COMMAND HAS A HALF SECOND BEFORE       #
#        CHECK THE OUTPUT AND PASS TO NEXT COMMAND. DEFINING THIS FIELD, THE WAIT TIME CAN BE SET TO THE    #
#        DESIRED VALUE.                                                                                     #
#                                                                                                           #
#  -- EXAMPLE OF A TEST:                                                                                    #
#  exampleCommandList: list[Command] = [                                                                    #
#      Command("apb conf show", "SHOWING THE APB CONF", CMD_OK)                                             #
#      Command("sdcard info", "SHOWING THE SD INFO", CMD_OK, 0.1)                                           #
#  ]                                                                                                        #
#                                                                                                           #
#############################################################################################################


CMD_OK = 0
CMD_NOT_FOUND = 1
CMD_NOT_FOUND_W_HELP = 2
CMD_ERR_UNKNOWN = 3
CMD_ERR_NO_MAIN = 4
CMD_ERR_NO_PARAM = 5
CMD_ERR_WRONG_PARAM = 6
CMD_ERR_INTL_FAIL = 7

POWER_DELAY = 8
EVENT_DELAY = 4

class Command:
    """ Base class of a command """

    key: str
    description: str
    expectedResult: int

    waitTime: float

    def __init__(self, key, description, expectedResult, waitTime: float = 0.5):
        self.key = key
        self.description = description
        self.expectedResult = expectedResult
        self.waitTime = waitTime

# File system test command list
fileSystemCommandList: list[Command] = [
    # MKDIR command
    Command("mkdir testfolder", "MKDIR GOOD FOLDER NAME TEST", CMD_OK),
    Command("mkdir testfolder", "MKDIR EXISTING FOLDER NAME TEST", CMD_ERR_INTL_FAIL),
    Command("mkdir", "MKDIR NO FOLDER NAME TEST", CMD_ERR_NO_PARAM),
    # LS command
    Command("ls", "LS NO PARAMETERS TEST", CMD_OK),
    Command("ls testfolder", "LS DIRECTORY TEST", CMD_OK),
    Command("ls testfolder2", "LS BAD DIRECTORY TEST", CMD_ERR_WRONG_PARAM),
    Command("ls testfolder -r", "LS -r DIRECTORY TEST", CMD_OK),
    Command("ls testfolder -h", "LS -h DIRECTORY TEST", CMD_OK),
    Command("ls testfolder -r-h", "LS -r-h DIRECTORY TEST", CMD_OK),
    Command("ls -r", "LS -r AS PARAM TEST", CMD_OK),
    Command("ls -h", "LS -h AS PARAM TEST", CMD_OK),
    # CD command
    Command("cd testfolder", "CD EXISTING FOLDER", CMD_OK),
    Command("cd testfolder2", "CD NON-EXISTING FOLDER", CMD_ERR_INTL_FAIL),
    Command("cd", "CD NO FOLDER", CMD_ERR_NO_PARAM),
    Command("cd /", "CD ROOT DIRECTORY", CMD_OK),
    # RENAME command
    Command("rename otherfolder otherfolder2", "RENAME BAD OLD FOLDER", CMD_ERR_WRONG_PARAM),
        # Create temporal folder to continue testing (1)
    Command("mkdir otherfolder", "TEMP FOLDER CREATION (1)", CMD_OK),
    Command("mkdir otherfolder2", "TEMP FOLDER CREATION (2)", CMD_OK),
    Command("rename otherfolder otherfolder2", "RENAME EXISTING NEW FOLDER", CMD_OK),
        # Create temporal folder to continue testing (2)
    Command("mkdir otherfolder", "TEMP FOLDER CREATION (3)", CMD_OK),
    Command("mkdir otherfolder2/otherfolder3", "TEMP FOLDER CREATION (4)", CMD_OK),
    Command("rename otherfolder otherfolder2", "RENAME EXISTING NEW FOLDER WITH ITEMS", CMD_ERR_INTL_FAIL),
    Command("rename testfolder testfolder2", "RENAME GOOD FOLDERS", CMD_OK),
    Command("rename testfolder2", "RENAME NO NEW FOLDER", CMD_ERR_NO_PARAM),
    # RMDIR command
    Command("rmdir testfolder", "RMDIR NON-EXISTING FOLDER", CMD_ERR_INTL_FAIL),
    Command("rmdir otherfolder", "RMDIR EXISTING FOLDER", CMD_OK),
    Command("rmdir otherfolder2", "RMDIR EXISTING FOLDER WITH ITEMS", CMD_ERR_INTL_FAIL),
    Command("rmdir", "RMDIR NO FOLDER", CMD_ERR_NO_PARAM),
    # MKFILE command
    Command("mkfile testfile.dat This_is_a_test", "MKFILE FILE CREATION TEST", CMD_OK),
    Command("mkfile", "MKFILE NO PARAM + VALUE TEST", CMD_ERR_NO_PARAM),
    Command("mkfile testfile.dat", "MKFILE NO VALUE TEST", CMD_ERR_NO_PARAM),
    # CAT command
    Command("cat", "CAT NO FILE", CMD_ERR_NO_PARAM),
    Command("cat testfile.dat", "CAT EXISTING FILE", CMD_OK),
    # RM command
    Command("rm test.dat", "RM NON-EXISTING FILE", CMD_ERR_INTL_FAIL),
    Command("rm testfile.dat", "RM EXISTING FILE", CMD_OK),
    Command("rm", "RM NO FILE", CMD_ERR_NO_PARAM),
    # RMALL command
    Command("rmall", "RMALL NO FOLDER", CMD_ERR_NO_PARAM),
    Command("rmall testfolder", "RMALL BAD FOLDER", CMD_ERR_WRONG_PARAM),
    Command("rmall otherfolder2", "RMDIR NORMAL FOLDER (1)", CMD_OK),
    Command("rmall testfolder2", "RMDIR NORMAL FOLDER (2)", CMD_OK),
    Command("rmall logs", "RMDIR LOGS FOLDER", CMD_OK),
]

# APB test command list
apbCommandList: list[Command] = [
    # APB ALARM
    Command("apb alarm temp_warning", "APB ALARM TEMP WARNING TEST", CMD_OK),
    Command("apb alarm temp_alarm", "APB ALARM TEMP ALARM TEST", CMD_OK),
    Command("apb alarm show", "APB ALARM TEMP SHOW TEST", CMD_OK),
    Command("apb alarm", "APB ALARM NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb alarm wrong_param", "APB ALARM WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # APB EEPROM command
    Command("apb eeprom dump", "APB EEPROM DUMP TEST", CMD_OK),
        # Command("apb eeprom erase", "APB EEPROM ERASE TEST", CMD_OK),
    Command("apb eeprom", "APB EEPROM NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb eeprom wrong_param", "APB EEPROM WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # APB INPUTMODE command
    Command("apb inputmode fs_usb", "APB INPUTMODE FS+USB TEST", CMD_OK),
    Command("apb inputmode fs", "APB INPUTMODE FS TEST", CMD_OK),
    Command("apb inputmode usb", "APB INPUTMODE USB TEST", CMD_OK),
    Command("apb inputmode sine", "APB INPUTMODE SINE TEST", CMD_OK),
    Command("apb inputmode mute", "APB INPUTMODE MUTE TEST", CMD_OK),
    Command("apb inputmode sd", "APB INPUTMODE SD TEST", CMD_OK),
        # Command("apb inputmode fs_sd", "APB INPUTMODE FS+SD TEST", CMD_OK),
    Command("apb inputmode", "APB INPUTMODE NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb inputmode wrong_param", "APB INPUTMODE WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # APB LT8390 command
    Command("apb lt8390 disable", "APB LT8390 DISABLE TEST", CMD_OK),
    Command("apb lt8390 enable", "APB LT8390 ENABLE TEST", CMD_OK),
    Command("apb lt8390 status", "APB LT8390 STATUS TEST", CMD_OK),
    Command("apb lt8390 ismon", "APB LT8390 ISMON TEST", CMD_OK),
    Command("apb lt8390", "APB LT8390 NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb lt8390 wrong_param", "APB LT8390 WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # APB TAS3251 command
    Command("apb tas3251 power off", "APB TAS3251 POWER OFF TEST", CMD_OK),
    Command("apb tas3251 power on", "APB TAS3251 POWER ON TEST", CMD_OK),
    Command("apb tas3251 mute off", "APB TAS3251 MUTE OFF TEST", CMD_OK),
    Command("apb tas3251 mute on", "APB TAS3251 MUTE ON TEST", CMD_OK),
    Command("apb tas3251 volume 50", "APB TAS3251 VOLUME TEST", CMD_OK),
        # Reset the TAS3251 for default settings
    Command("apb tas3251 reset", "APB TAS3251 RESET TEST (1)", CMD_OK),
    Command("apb tas3251 test filters/c1f_c2f.bin", "APB TAS3251 TEST FILTER TEST", CMD_OK),
        # Reset the TAS3251 for default settings
    Command("apb tas3251 reset", "APB TAS3251 RESET TEST (2)", CMD_OK),
    Command("apb tas3251", "APB TAS3251 NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb tas3251 wrong_param", "APB TAS3251 WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # APB FILTER command
    Command("apb filter check_path all", "APB FILTER CHECK ALL FILTERS TEST", CMD_OK),
    Command("apb filter check_path c1f_c2f", "APB FILTER CHECK C1F_C2F FILTER TEST", CMD_OK),
    Command("apb filter check_path c1f_c2t", "APB FILTER CHECK C1F_C2T FILTER TEST", CMD_OK),
    Command("apb filter check_path c1t_c2f", "APB FILTER CHECK C1T_C2F FILTER TEST", CMD_OK),
    Command("apb filter check_path c1t_c2t", "APB FILTER CHECK C1T_C2T FILTER TEST", CMD_OK),
    Command("apb filter check_path diag", "APB FILTER CHECK DIAG FILTER TEST", CMD_OK),
    Command("apb filter", "APB TAS3251 NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb filter wrong_param", "APB TAS3251 WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # APB MONITOR command
    Command("apb monitor disable", "APB MONITOR DISABLE TEST", CMD_OK),
    Command("apb monitor enable", "APB MONITOR ENABLE TEST", CMD_OK),
    Command("apb monitor timer 500", "APB MONITOR TIMER TEST", CMD_OK),
    Command("apb monitor show", "APB MONITOR SHOW TEST", CMD_OK),
    Command("apb monitor", "APB MONITOR NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb monitor wrong_param", "APB MONITOR WRONG PARAM TEST", CMD_ERR_WRONG_PARAM)
]

# Event test command list
eventCommandList: list[Command] = [
        # Prepare the APB for the test
    Command("event power on", "PREPARING THE APB FOR THE TEST (1)", None, POWER_DELAY),
    Command("event beamwidth narrow", "PREPARING THE APB FOR THE TEST (2)", None, EVENT_DELAY),
    Command("event voiceboost off", "PREPARING THE APB FOR THE TEST (3)", None, EVENT_DELAY),
    # EVENT POWER command
    Command("event power off", "EVENT POWER OFF TEST", CMD_OK, POWER_DELAY),
    Command("event power off", "EVENT POWER OFF TEST WHEN POWERED OFF", CMD_ERR_INTL_FAIL),
    Command("event power on", "EVENT POWER ON TEST", CMD_OK, POWER_DELAY),
    Command("event power on", "EVENT POWER ON TEST WHEN POWERED ON", CMD_ERR_INTL_FAIL),
    Command("event power", "EVENT POWER NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("event power wrong_param", "EVENT POWER WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # EVENT BEAMWIDTH command
    Command("event beamwidth wide", "EVENT BEAMWIDTH WIDE TEST", CMD_OK, EVENT_DELAY),
    Command("event beamwidth wide", "EVENT BEAMWIDTH WIDE TEST WHEN WIDE", CMD_ERR_INTL_FAIL),
    Command("event beamwidth narrow", "EVENT BEAMWIDTH NARROW TEST", CMD_OK, EVENT_DELAY),
    Command("event beamwidth narrow", "EVENT BEAMWIDTH NARROW TEST WHEN NARROW", CMD_ERR_INTL_FAIL),
    Command("event beamwidth", "EVENT BEAMWIDTH NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("event beamwidth wrong_param", "EVENT BEAMWIDTH WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # EVENT VOICEBOOST command
    Command("event voiceboost on", "EVENT VOICEBOOST ON TEST", CMD_OK, EVENT_DELAY),
    Command("event voiceboost on", "EVENT VOICEBOOST ON TEST WHEN ON", CMD_ERR_INTL_FAIL),
    Command("event voiceboost off", "EVENT VOICEBOOST OFF TEST", CMD_OK, EVENT_DELAY),
    Command("event voiceboost off", "EVENT VOICEBOOST OFF TEST WHEN OFF", CMD_ERR_INTL_FAIL),
    Command("event voiceboost", "EVENT VOICEBOOST NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("event voiceboost wrong_param", "EVENT VOICEBOOST WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # EVENT TEST command
    Command("event test", "EVENT TEST TEST", CMD_OK)
]

# Miscellaneous test command list
miscCommandList: list[Command] = [
    # DEBUG command
    Command("debug get", "GET DEBUG LEVEL TEST", CMD_OK),
    Command("debug set 10", "SET DEBUG INVALID LEVEL TEST", CMD_OK),
    Command("debug set 6", "SET DEBUG VALID LEVEL TEST", CMD_OK),
    Command("debug", "DEBUG NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("debug wrong_param", "DEBUG WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # FILELOG command
    Command("filelog get", "GET FILELOG LEVEL TEST", CMD_OK),
    Command("filelog set 10", "SET FILELOG INVALID LEVEL TEST", CMD_OK),
    Command("filelog set 6", "SET FILELOG VALID LEVEL TEST", CMD_OK),
    Command("filelog disable", "DISABLE FILELOG TEST", CMD_OK),
    Command("filelog enable", "ENABLE FILELOG TEST", CMD_OK),
    Command("filelog", "FILELOG NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("filelog wrong_param", "FILELOG WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    # STATUS command
    Command("status", "STATUS TEST", CMD_OK),
    # RTC command
    Command("rtc get", "GET RTC TIMESTAMP TEST", CMD_OK),
    Command("rtc set 2021-08-15 10:00:00", "SET RTC VALID TIMESTAMP TEST", CMD_OK),
    Command("rtc set wrong_timestamp", "SET RTC INVALID TIMESTAMP TEST", CMD_ERR_WRONG_PARAM),
    Command("rtc", "RTC NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("rtc wrong_param", "RTC WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
]

# A list of all the parameters and values of the configuration file
setParamValueList: list[tuple] = [
    ("console_debug_level", "6"),
    ("file_verbosity_level", "6"),
    ("enable_console_debug", "1"),
    ("enable_filelog", "1"),
    ("set_logdir", "logs/"),
    ("log_file_prefix", "APB2_log"),
    ("set_audiodir", "audio/"),
    ("filter_set", "filter"),
    ("c1f_c2f", "filters/c1f_c2f.bin"),
    ("c1t_c2t", "filters/c1f_c2t.bin"),
    ("c1t_c2f", "filters/c1t_c2f.bin"),
    ("c1f_c2t", "filters/c1t_c2t.bin"),
    ("diag", "filters/diag.bin"),
    ("collector_port", "0"),
    ("collector_baudrate", "921600"),
    ("collector_cycle", "5000"),
    ("collector_enable", "1"),
    ("delta_warning", "200"),
    ("alarm_reporting", "1"),
    ("warning", "3400"),
    ("warning_action", "0"),
    ("alarm_action", "0"),
    ("alarm", "3700"),
    ("mixer_1", "500"),
    ("mixer_2", "500"),
    ("mixer_3", "1000"),
    ("mixer_4", "1000"),
    ("master", "866"),
    ("channel_ana", "500"),
    ("channel_usb", "400"),
    ("fs_gain", "1000"),
    ("usb_gain", "500"),
    ("audio_fs_gain", "866"),
    ("sine_gain", "500"),
    ("input_usb_gain", "400"),
    ("input_ana_gain", "500"),
    ("usb_audio_enable", "2")
]

# Create a list with all the SET commands formatting each one with the corresponding parameter and value (default values)
tempList: list[Command] = []
for param, value in setParamValueList:
    tempList.append(Command("apb conf set {} {}".format(param, value), "SET APB CONFIGURATION {} PARAMETER TEST".format(param.upper()), CMD_OK, 0.1))

# APB configuration file test command list
apbConfCommandList: list[Command] = [
    # APB CONF SAVE command
    Command("apb conf save", "SAVE APB CONFIGURATION TEST", CMD_OK),
    # APB CONF APPLY command
    Command("apb conf apply", "APPLY APB CONFIGURATION TEST", CMD_OK),
    # APB CONF SHOW command
    Command("apb conf show", "SHOW APB CONFIGURATION TEST", CMD_OK),
    # APB CONF SETFILTERSAVE command
    Command("apb conf setfiltersave", "SETFILTERSAVE APB CONFIGURATION TEST", CMD_OK),
]

# Add all the APB CONF SET commands
apbConfCommandList += tempList

# Print the total list
# for item in apbConfCommandList:
#     print("Command key: {}\nDescription: {}\nExpected result: {}\n".format(item.key, item.description, item.expectedResult))

# APB gain test command list
apbGainCommandList: list[Command] = [
    # APB GAIN MIXER command
    Command("apb gain mixer 1 0 0.4", "APB GAIN MIXER VALID VALUE TEST", CMD_OK),
    Command("apb gain mixer 1 0 10", "APB GAIN MIXER INVALID VALUE TEST", CMD_OK),
    Command("apb gain mixer", "APB GAIN MIXER NO PARAM + VALUE TEST", CMD_ERR_NO_PARAM),
    Command("apb gain mixer wrong_param", "APB GAIN MIXER NO CH + VALUE TEST", CMD_ERR_NO_PARAM),
    Command("apb gain mixer wrong_param 0 0.4", "APB GAIN MIXER WRONG MIXER TEST", CMD_ERR_WRONG_PARAM),
    Command("apb gain mixer 1 0", "APB GAIN MIXER NO VALUE TEST", CMD_ERR_NO_PARAM),
    # APB GAIN AMP command
    Command("apb gain amp usb_in 0.2", "APB GAIN AMP VALID VALUE TEST", CMD_OK),
    Command("apb gain amp usb_in 10", "APB GAIN AMP INVALID VALUE TEST", CMD_OK),
    Command("apb gain amp", "APB GAIN AMP NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb gain amp wrong_param", "APB GAIN AMP WRONG PARAM TEST", CMD_ERR_WRONG_PARAM),
    Command("apb gain amp 1 wrong_value", "APB GAIN AMP WRONG VALUE TEST", CMD_ERR_WRONG_PARAM),
    # APB GAIN SHOW command
    Command("apb gain show all", "APB GAIN AMP VALID VALUE TEST", CMD_OK),
    Command("apb gain show", "APB GAIN AMP NO PARAM TEST", CMD_ERR_NO_PARAM),
    Command("apb gain show wrong_PARAM", "APB GAIN AMP WRONG PARAM TEST", CMD_ERR_WRONG_PARAM)
]
