
#!python3
"""
System test diagnosys

Connect with an APB board and send it CLI commands to execute the diagnosys and analyzes the output.

Created on 09 AUG 2021

@authors{ isierra@genasys.com, rpalacio@genasys.com }
@version 2.0

history

"""

from datetime import datetime
import serial
from serial.tools import list_ports
from time import sleep
import threading
from tests import *

TEST_PASSED = True
TEST_FAILED = False

# Error code list
errorCodeMsgList: list[str] = [
    "Command OK",
    "Command not found",
    "Command not found - showing help message",
    "Error unknown",
    "No main action",
    "Not enough parameters",
    "Wrong parameters",
    "Internal error"
]

global isPortOpened
isPortOpened: bool = False
global serialPort
serialPort: serial.Serial = None
global testRunningFlag
testRunningFlag: bool = False
global errorCode
errorCode: int = 0
global testResult

global failCommandsList
failCommandsList: list[tuple[Command, int]] = []
global totalFailCommandList
totalFailCommandList: list[tuple[Command, int]] = []


def findErrorCode(msg: str):
    """
    Find a command error message and convert the code error into an integer value
    """
    # Find the error code message
    initIndex = msg.find("ERROR CODE :: ")
    # Check if the message has been found
    if(initIndex != -1):
        # Extract the error code number and return it converted into an integer
        return int(msg[initIndex + 14:])
    # If the message is not found, return None
    else:
        return None


def connectSerialPort(comPort: str, baudrate: int = 921600, type: str = "console"):
    """
    Connect a serial port to the board
    """
    global isPortOpened
    global serialPort

    isPortOpened = False

    try:
        # Check if the COM is valid
        if comPort.startswith("COM") != True:
            return

        # Create a new serial port with the specified port and baudrate
        serialPort = serial.Serial(comPort, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS,
                                   stopbits=serial.STOPBITS_ONE, xonxoff=0, rtscts=0, dsrdtr=0, baudrate=baudrate)
        isPortOpened = True

        # Check if any port is running
        for t in threading.enumerate():
            if type == "console" and t.name == "_cliconsole_":
                return

        # Add a new thread to start the serial communication
        if type == "console":
            threadUART = threading.Thread(target=cliConsole, name="_cliconsole_")
            threadUART.daemon = True
            threadUART.start()
        elif type == "monitor":
            "TO DO"
            # threading.Thread(target=, name="_monitor_").start()

    # Catch the serial exception error
    except (OSError, serial.SerialException):
        print("The {} port cannot be started...".format(comPort))
        serialPort = None


def cliConsole():
    """
    Thread to read continuous data from the serial port and analyze it for errors
    """
    global isPortOpened
    global serialPort
    global testRunningFlag
    global errorCode
    global testResult

    input: str = ""

    # Check if serial port is running
    while isPortOpened == True:
        # Read serial port byte by byte
        ch = serialPort.read()
        ch = ch.decode('utf-8')

        # Check if the character is a newline character
        if ch == '\r' or ch == '\n':
            if len(input) > 1:
                print("{}: >> {}".format(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3],input))

            # Search an error code message
            tempError = findErrorCode(input)
            if tempError is not None:
                # If an error code message is found, store it in the global variable
                errorCode = tempError

            input = ""

        # Otherwise, add the character to the current command line
        else:
            input += ch

        # Check if the CLI prompt has been received
        if input.find("CLI> ") != -1:
            print("Board waiting for a command")
            input = ""
            testRunningFlag = False

        sleep(0.001)


def do_test(command: str, description: str = "", delay: float = 0.50, wait: float = 0.01):
    global serialPort
    global testRunningFlag
    global errorCode
    global testResult

    # Waits until a CLI prompt is received
    while testRunningFlag:
        pass

    # Launch the test
    print(description, end='' if description == "" else '\n')
    testRunningFlag = True
    serialPort.write(bytes(command + "\n", encoding="utf-8"))
    sleep(delay)
    while testRunningFlag:
        sleep(wait)

    if errorCode is not None:
        print("ERROR CODE: {} -- {}".format(errorCode,
              errorCodeMsgList[errorCode]))

    # Return a dict with both data
    return errorCode


def execute_command(command: Command):
    """
    Executes a command and analyzes the error code obtained.

    If the error code does not match the expected, then show a message and add the command code to the fail
    commands list.
    """
    global failCommandsList

    # Execute the command and obtain the results
    result = do_test(command.key, command.description, command.waitTime)

    if command.expectedResult != None:
        # Check if the error code match the expected
        if(result != command.expectedResult):
            print(" ## Command failed:\n >> Expected result: {} -- {}\n    Received result: {} -- {}".format(command.expectedResult,
                                                                                                             errorCodeMsgList[command.expectedResult].upper(), result, errorCodeMsgList[result].upper()))
            # Add the command to the failed command list
            failCommandsList.append((command, result))

        else:
            print(" ## Command test OK")
    else:
        print(" ## Command test done without checking the output...")


def execute_test(description: str, commandList: list[Command]):
    """
    Performs a test in the board, testing a set of commands
    """
    global failCommandsList
    global totalFailCommandList

    # Clear the failed commands list
    failCommandsList.clear()

    print("Executing: '" + description + "'")

    # Execute each command in the list
    for cmd in commandList:
        print("Launching: '" + cmd.key + "'")
        execute_command(cmd)

    # Prints the failed commands and the test result
    nFailedCommands = len(failCommandsList)
    if nFailedCommands == 0:
        print("\n:: Test passed ::")
    else:
        print("\n{} {} has failed:".format(nFailedCommands, "command" if nFailedCommands == 1 else "commands"))
        for item, result in failCommandsList:
            print(" - " + item.description, 
                  "-> Expected output: {} ({}), got {} ({})".format(item.expectedResult,
                  errorCodeMsgList[item.expectedResult].upper(), 
                  result, errorCodeMsgList[result].upper()))
        print("\n:: Test failed ::")
        
        # Append the current fail list to the total fail list
        totalFailCommandList += failCommandsList


def main():
    """
    Main function of the script
    """
    global serialPort
    global testRunningFlag
    global errorCode
    global testResult
    global failCommandsList
    global totalFailCommandList

    # Initialize the serial communications
    comPort = autoDetectComPort()
    connectSerialPort(comPort)
    if serialPort is None:
        print("Error while opening the serial port")
        return

    ##########################
    ### Performs the tests ###
    ##########################

    for iteration in range(1, 21):
        serialPort.write(bytes("hello world\n", encoding="utf-8"))
        print("{}: Message sent...".format(iteration))
        sleep(0.5)

    # execute_test("File-system test command list", fileSystemCommandList)
    # execute_test("APB test command list", apbCommandList)
    # execute_test("Event test command list", eventCommandList)
    # execute_test("Miscellaneous test command list",miscCommandList)
    # execute_test("APB configuration file test command list",apbConfCommandList)
    # execute_test("APB gain test command list",apbGainCommandList)

    ################################
    ### Display the failed tests ###
    ################################

    # End the tests
    print("\n:: TEST ENDED ::")

    # nFailedCommands = len(totalFailCommandList)
    # if nFailedCommands != 0:
              
    #     print("\n>> ################# <<\n>> ALL TESTS RESULTS <<\n>> ################# <<")
    #     print("{} {} has failed:".format(nFailedCommands, "command" if nFailedCommands == 1 else "commands"))
    #     for item, result in totalFailCommandList:
    #         print(" - " + item.description, 
    #               "-> Expected output: {} ({}), got {} ({})".format(item.expectedResult,
    #               errorCodeMsgList[item.expectedResult].upper(), 
    #               result, errorCodeMsgList[result].upper()))
        
    #     return TEST_FAILED
    
    # return TEST_PASSED


def autoDetectComPort():
    # Teensy USB serial microcontroller program ID data
    VENDOR_ID = "0403"
    PRODUCT_ID = "6001"

    # Search the available ports
    teensyPort: str = ""
    print("Searching ports...")
    ports = list(list_ports.comports())
    for port in ports:
        if(port[2].startswith("USB VID:PID={}:{}".format(VENDOR_ID, PRODUCT_ID))):
            if teensyPort != "":
                teensyPort = "OVERLOAD"
                break
            else:
                teensyPort = port[0]

    if teensyPort == "":
        print("No Teensy detected...")
    elif teensyPort == "OVERLOAD":
        print("WARNING! Various Teensy's detected... Please, connect only one.")
    else:
        print("Teensy connected at: {}".format(teensyPort))

    return teensyPort
                

if __name__ == '__main__':
    main()
