#include <Arduino.h>
#include <SoftPWM.h>

char tempBuffer[100] = {0};
const int MaxInput = 100;
long prevMillis = 0;
int loopDelay = 1000;

// ---------------------------------------------------------------------------
void setup()
// ---------------------------------------------------------------------------
{
  Serial1.begin(921600);
  while (!Serial1)
    ; // wait for connection to terminal emulator
  Serial1.println("Enter something");

  SoftPWMBegin();
  SoftPWMSet(13, 0);
  SoftPWMSetFadeTime(13, 250, 250);
}

// ---------------------------------------------------------------------------
void loop()
// ---------------------------------------------------------------------------
{
  SoftPWMSet(13, 255);
  delay(500);
  SoftPWMSet(13, 0);
  delay(500);
}
// ---------------------------------------------------------------------------
void serialEvent1(void)
// ---------------------------------------------------------------------------
{
  static unsigned int input_pos = 0;
  //Serial1.println("+"); // debug so we know this has been called
  while (Serial1.available())
  {
    char inByte = (char)Serial1.read(); 
    switch (inByte)
    {
    case '\n':                   // end of text
      tempBuffer[input_pos] = 0; // terminating null byte
      Serial1.println(tempBuffer);
      input_pos = 0;
      break;

    default:
      // only allow alphabet and punctuation chrs
      if (inByte >= 32 && inByte <= 126)
      {
        // keep adding if not full ... allow for terminating null byte
        if (input_pos < (MaxInput - 1))
          tempBuffer[input_pos++] = inByte;
      } // end if((inByte ....
      break;
    }
  }
}